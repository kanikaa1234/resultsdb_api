Name:           python-resultsdb_api
Version:        1.3.0
Release:        1%{?dist}
Summary:        Interface api to ResultsDB

License:        GPLv2+
URL:            https://bitbucket.org/fedoraqa/resultsdb_api
Source0:        https://qa.fedoraproject.org/releases/resultsdb_api/resultsdb_api-%{version}.tar.gz

BuildArch:      noarch

Requires:       python-requests
Requires:       python-simplejson
BuildRequires:  git
BuildRequires:  pytest
BuildRequires:  python2-devel
BuildRequires:  python-dingus
BuildRequires:  python-pytest-cov
BuildRequires:  python-requests
BuildRequires:  python-setuptools
BuildRequires:  python-simplejson
BuildRequires:  python-virtualenv

%{?python_provide:%python_provide python-resultsdb_api}

%description
Interface api to ResultsDB

%prep
%setup -q -n resultsdb_api-%{version}

%check
make test

%build
%{__python2} setup.py build

%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%files
%doc README.md
%license LICENSE
%{python2_sitelib}/resultsdb_api.*
%{python2_sitelib}/*.egg-info

%changelog
* Thu Nov 3 2016 Tim Flink <tflink@fedoraproject.org> - 1.3.0-1
- add support for resultsdb 2.0

* Wed May 25 2016 Martin Krizek <mkrizek@redhat.com> - 1.2.2-3
- remove not needed custom python_sitelib macro

* Tue May 24 2016 Martin Krizek <mkrizek@redhat.com> - 1.2.2-2
- rename to python-resultsdb_api (obsolete resultsdb_api)
- add python_provide
- add LICENSE file
- add check

* Wed Jul 8 2015 Martin Krizek <mkrizek@redhat.com> - 1.2.2-1
- Remove trailing slashes from url before it's used
- Add missing python-simplejson dependency

* Thu Apr 9 2015 Tim Flink <tflink@fedoraproject.org> - 1.2.1-1
- added option for retrieving job data after update_job (T456)

* Wed Apr 1 2015 Tim Flink <tflink@fedoraproject.org> - 1.2.0-1
- added logging capability, logging response errors
- added UUID support for execdb integration

* Fri May 16 2014 Tim Flink <tflink@fedoraproject.org> - 1.1.0-1
- Releasing resultsdb_api 1.1.0

* Fri Apr 25 2014 Tim Flink <tflink@fedoraproject.org> - 1.0.2-1
- bugfixes from upstream

* Fri Apr 11 2014 Tim Flink <tflink@fedoraproject.org> - 1.0.1-1
- initial packaging
